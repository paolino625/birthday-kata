package paolo.calcagni.birthdayKata;

import static org.assertj.core.api.Assertions.assertThat;

import com.dumbster.smtp.SimpleSmtpServer;
import com.dumbster.smtp.SmtpMessage;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MessageServiceRealTest {
  private static final int NONSTANDARD_PORT = 9999;
  private final MessageServiceReal messageServiceReal =
      new MessageServiceReal("localhost", NONSTANDARD_PORT);
  private SimpleSmtpServer smtpServer;

  @BeforeEach
  public void setUp() {
    try {
      smtpServer = SimpleSmtpServer.start(NONSTANDARD_PORT);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @AfterEach
  public void tearDown() {
    smtpServer.stop();
  }

  @Test
  public void invioMessaggioEmail() throws Exception {

    messageServiceReal.sendMessage("Test", "Ciao! Tanti auguri!", "receiver@there.com");

    List<SmtpMessage> emails = smtpServer.getReceivedEmails();
    assertThat(emails.size()).isEqualTo(1);
    SmtpMessage email = emails.get(0);
    assertThat(email.getHeaderValue("Subject")).isEqualTo("Test");
    assertThat(email.getBody()).isEqualTo("Ciao! Tanti auguri!");
    assertThat(email.getHeaderValue("To")).isEqualTo("receiver@there.com");
  }
}
