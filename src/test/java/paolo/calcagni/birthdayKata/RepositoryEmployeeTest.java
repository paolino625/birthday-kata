package paolo.calcagni.birthdayKata;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import java.io.BufferedReader;
import java.io.IOException;
import org.junit.jupiter.api.Test;

public class RepositoryEmployeeTest {

  @Test
  public void inizializzaRepository_test() throws IOException {

    // SETUP

    BufferedReader in = mock(BufferedReader.class);

    when(in.readLine())
        .thenReturn(
            "last_name, first_name, date_of_birth, email",
            "Calcagni, Paolo, 1997-01-03, paolo.calcagni@studenti.unimi.it",
            "Mary, Ann, 1975-03-11, mary.ann@foobar.com",
            null);

    RepositoryEmployeeReal repositoryEmployee = new RepositoryEmployeeReal();

    // EXECUTE

    repositoryEmployee.inizializzaRepository(in);

    // VERIFY

    assertThat(repositoryEmployee.listaEmployeeRepository())
        .isEqualTo(
            "Nome: Paolo\n"
                + "Cognome: Calcagni\n"
                + "Data di Nascita: 1997/1/3\n"
                + "Mail: paolo.calcagni@studenti.unimi.it\n"
                + "Nome: Ann\n"
                + "Cognome: Mary\n"
                + "Data di Nascita: 1975/3/11\n"
                + "Mail: mary.ann@foobar.com\n");
  }
}
