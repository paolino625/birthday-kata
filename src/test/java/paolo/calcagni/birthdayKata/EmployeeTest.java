package paolo.calcagni.birthdayKata;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class EmployeeTest {

  @Test
  public void costruttoreEmployee_nome_test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "2020-04-03", "paolo.calcagni@studenti.unimi.it");

    paolo.calcagni.birthdayKata.asserzioniGenerate.Assertions.assertThat(employee).hasNome("Paolo");
  }

  @Test
  public void costruttoreEmployee_cognome_test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "2020-04-03", "paolo.calcagni@studenti.unimi.it");

    paolo
        .calcagni
        .birthdayKata
        .asserzioniGenerate
        .Assertions
        .assertThat(employee)
        .hasCognome("Calcagni");
  }

  @Test
  public void costruttoreEmployee_dataNascita_test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "2020-04-03", "paolo.calcagni@studenti.unimi.it");

    paolo
        .calcagni
        .birthdayKata
        .asserzioniGenerate
        .Assertions
        .assertThat(employee)
        .hasDataNascita("2020/4/3");
  }

  @Test
  public void costruttoreEmployee_email_test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "2020-04-03", "paolo.calcagni@studenti.unimi.it");

    paolo
        .calcagni
        .birthdayKata
        .asserzioniGenerate
        .Assertions
        .assertThat(employee)
        .hasEmail("paolo.calcagni@studenti.unimi.it");
  }

  @Test
  public void getNome_test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "2020-04-03", "paolo.calcagni@studenti.unimi.it");

    Assertions.assertThat(employee.getNome()).isEqualTo("Paolo");
  }

  @Test
  public void getCognome_test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "2020-04-03", "paolo.calcagni@studenti.unimi.it");

    Assertions.assertThat(employee.getCognome()).isEqualTo("Calcagni");
  }

  @Test
  public void getGiornoNascita_test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "2020-04-03", "paolo.calcagni@studenti.unimi.it");

    Assertions.assertThat(employee.getGiornoNascita()).isEqualTo(3);
  }

  @Test
  public void getMeseNascita_test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "2020-04-03", "paolo.calcagni@studenti.unimi.it");

    Assertions.assertThat(employee.getMeseNascita()).isEqualTo(4);
  }

  @Test
  public void getEmail_test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "2020-04-03", "paolo.calcagni@studenti.unimi.it");

    Assertions.assertThat(employee.getEmail()).isEqualTo("paolo.calcagni@studenti.unimi.it");
  }

  @Test
  public void compleannoOggi_falso_giornoemesediversi_test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "1789-01-24", "paolo.calcagni@studenti.unimi.it");

    LocalDate dataOdierna = LocalDate.parse("2020-04-03");
    assertThat(employee.compleannoOggi(dataOdierna)).isFalse();
  }

  @Test
  public void compleannoOggi_falso_giornodiverso_test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "1789-04-03", "paolo.calcagni@studenti.unimi.it");

    LocalDate dataOdierna = LocalDate.parse("2020-04-04");
    Assertions.assertThat(employee.compleannoOggi(dataOdierna)).isFalse();
  }

  @Test
  public void compleannoOggi_falso_mesediverso_test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "1789-04-03", "paolo.calcagni@studenti.unimi.it");

    LocalDate dataOdierna = LocalDate.parse("2020-05-03");
    Assertions.assertThat(employee.compleannoOggi(dataOdierna)).isFalse();
  }

  @Test
  public void compleannoOggi_vero_test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "1789-04-03", "paolo.calcagni@studenti.unimi.it");

    LocalDate dataOdierna = LocalDate.parse("2020-04-03");
    Assertions.assertThat(employee.compleannoOggi(dataOdierna)).isTrue();
  }

  @Test
  public void toString_Test() {

    Employee employee =
        new Employee("Paolo", "Calcagni", "1789-04-03", "paolo.calcagni@studenti.unimi.it");

    assertThat(employee.toString())
        .isEqualTo("Paolo Calcagni 1789/4/3 paolo.calcagni@studenti.unimi.it");
  }
}
