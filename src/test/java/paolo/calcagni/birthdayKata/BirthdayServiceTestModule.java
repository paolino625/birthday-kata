package paolo.calcagni.birthdayKata;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import java.util.ArrayList;

public class BirthdayServiceTestModule extends AbstractModule {

  @Provides
  @Singleton
  MessageService messageService() {
    return mock(MessageService.class);
  }

  @Provides
  RepositoryEmployee fornisciRepository() {

    ArrayList<Employee> listaEmployee = new ArrayList<>();
    listaEmployee.add(
        new Employee("Paolo", "Calcagni", "1997-01-03", "paolo.calcagni@studenti.unimi.it"));
    listaEmployee.add(
        new Employee("Paolo", "Calcagni", "1997-01-03", "paolo.calcagni@studenti.unimi.it"));
    listaEmployee.add(new Employee("Paolo", "Mary", "1975-03-11", "mary.ann@foobar.com"));

    RepositoryEmployee repositoryEmployee = mock(RepositoryEmployee.class);
    when(repositoryEmployee.getListaEmployee()).thenReturn(listaEmployee);

    return repositoryEmployee;
  }
}
