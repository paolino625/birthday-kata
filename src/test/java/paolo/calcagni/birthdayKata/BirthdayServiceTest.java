package paolo.calcagni.birthdayKata;

import static org.mockito.Mockito.*;

import com.google.inject.Guice;
import com.google.inject.Injector;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;

public class BirthdayServiceTest {

  @Test
  public void sendGreetings_test() {

    // SETUP

    Injector injector = Guice.createInjector(new BirthdayServiceTestModule());
    MessageService messageServiceMock = injector.getInstance(MessageService.class);
    RepositoryEmployee repositoryEmployeeMock = injector.getInstance(RepositoryEmployee.class);

    BirthdayService birthdayService =
        new BirthdayService(messageServiceMock, repositoryEmployeeMock);

    LocalDate data = LocalDate.parse("2000-01-03");

    // EXECUTE

    birthdayService.sendGreetings(data);

    // VERIFY

    verify(messageServiceMock, times(2))
        .sendMessage(
            "Happy Birthday!", "Happy Birthday, dear Paolo", "paolo.calcagni@studenti.unimi.it");
    verify(messageServiceMock, times(0))
        .sendMessage("Happy Birthday!", "Happy Birthday, dear Mary", "mary.ann@foobar.com");
  }
}
