package paolo.calcagni.birthdayKata.asserzioniGenerate;

import org.assertj.core.api.AbstractObjectAssert;
import org.assertj.core.internal.Iterables;
import paolo.calcagni.birthdayKata.Employee;
import paolo.calcagni.birthdayKata.RepositoryEmployeeReal;

/** {@link RepositoryEmployeeReal} specific assertions - Generated by CustomAssertionGenerator. */
@javax.annotation.Generated(value = "assertj-assertions-generator")
public class KataRepositoryEmployeeAssert
    extends AbstractObjectAssert<KataRepositoryEmployeeAssert, RepositoryEmployeeReal> {

  /**
   * Creates a new <code>{@link KataRepositoryEmployeeAssert}</code> to make assertions on actual
   * RepositoryEmployee.
   *
   * @param actual the RepositoryEmployee we want to make assertions on.
   */
  public KataRepositoryEmployeeAssert(RepositoryEmployeeReal actual) {
    super(actual, KataRepositoryEmployeeAssert.class);
  }

  /**
   * An entry point for KataRepositoryEmployeeAssert to follow AssertJ standard <code>assertThat()
   * </code> statements.<br>
   * With a static import, one can write directly: <code>assertThat(myRepositoryEmployee)</code> and
   * get specific assertion with code completion.
   *
   * @param actual the RepositoryEmployee we want to make assertions on.
   * @return a new <code>{@link KataRepositoryEmployeeAssert}</code>
   */
  @org.assertj.core.util.CheckReturnValue
  public static KataRepositoryEmployeeAssert assertThat(RepositoryEmployeeReal actual) {
    return new KataRepositoryEmployeeAssert(actual);
  }

  /**
   * Verifies that the actual RepositoryEmployee's listaEmployee contains the given Employee
   * elements.
   *
   * @param listaEmployee the given elements that should be contained in actual RepositoryEmployee's
   *     listaEmployee.
   * @return this assertion object.
   * @throws AssertionError if the actual RepositoryEmployee's listaEmployee does not contain all
   *     given Employee elements.
   */
  public KataRepositoryEmployeeAssert hasListaEmployee(Employee... listaEmployee) {
    // check that actual RepositoryEmployee we want to make assertions on is not null.
    isNotNull();

    // check that given Employee varargs is not null.
    if (listaEmployee == null) failWithMessage("Expecting listaEmployee parameter not to be null.");

    // check with standard error message, to set another message call:
    // info.overridingErrorMessage("my error message");
    Iterables.instance().assertContains(info, actual.getListaEmployee(), listaEmployee);

    // return the current assertion for method chaining
    return this;
  }

  /**
   * Verifies that the actual RepositoryEmployee's listaEmployee contains the given Employee
   * elements in Collection.
   *
   * @param listaEmployee the given elements that should be contained in actual RepositoryEmployee's
   *     listaEmployee.
   * @return this assertion object.
   * @throws AssertionError if the actual RepositoryEmployee's listaEmployee does not contain all
   *     given Employee elements.
   */
  public KataRepositoryEmployeeAssert hasListaEmployee(
      java.util.Collection<? extends Employee> listaEmployee) {
    // check that actual RepositoryEmployee we want to make assertions on is not null.
    isNotNull();

    // check that given Employee collection is not null.
    if (listaEmployee == null) {
      failWithMessage("Expecting listaEmployee parameter not to be null.");
      return this; // to fool Eclipse "Null pointer access" warning on toArray.
    }

    // check with standard error message, to set another message call:
    // info.overridingErrorMessage("my error message");
    Iterables.instance().assertContains(info, actual.getListaEmployee(), listaEmployee.toArray());

    // return the current assertion for method chaining
    return this;
  }

  /**
   * Verifies that the actual RepositoryEmployee's listaEmployee contains <b>only</b> the given
   * Employee elements and nothing else in whatever order.
   *
   * @param listaEmployee the given elements that should be contained in actual RepositoryEmployee's
   *     listaEmployee.
   * @return this assertion object.
   * @throws AssertionError if the actual RepositoryEmployee's listaEmployee does not contain all
   *     given Employee elements.
   */
  public KataRepositoryEmployeeAssert hasOnlyListaEmployee(Employee... listaEmployee) {
    // check that actual RepositoryEmployee we want to make assertions on is not null.
    isNotNull();

    // check that given Employee varargs is not null.
    if (listaEmployee == null) failWithMessage("Expecting listaEmployee parameter not to be null.");

    // check with standard error message, to set another message call:
    // info.overridingErrorMessage("my error message");
    Iterables.instance().assertContainsOnly(info, actual.getListaEmployee(), listaEmployee);

    // return the current assertion for method chaining
    return this;
  }

  /**
   * Verifies that the actual RepositoryEmployee's listaEmployee contains <b>only</b> the given
   * Employee elements in Collection and nothing else in whatever order.
   *
   * @param listaEmployee the given elements that should be contained in actual RepositoryEmployee's
   *     listaEmployee.
   * @return this assertion object.
   * @throws AssertionError if the actual RepositoryEmployee's listaEmployee does not contain all
   *     given Employee elements.
   */
  public KataRepositoryEmployeeAssert hasOnlyListaEmployee(
      java.util.Collection<? extends Employee> listaEmployee) {
    // check that actual RepositoryEmployee we want to make assertions on is not null.
    isNotNull();

    // check that given Employee collection is not null.
    if (listaEmployee == null) {
      failWithMessage("Expecting listaEmployee parameter not to be null.");
      return this; // to fool Eclipse "Null pointer access" warning on toArray.
    }

    // check with standard error message, to set another message call:
    // info.overridingErrorMessage("my error message");
    Iterables.instance()
        .assertContainsOnly(info, actual.getListaEmployee(), listaEmployee.toArray());

    // return the current assertion for method chaining
    return this;
  }

  /**
   * Verifies that the actual RepositoryEmployee's listaEmployee does not contain the given Employee
   * elements.
   *
   * @param listaEmployee the given elements that should not be in actual RepositoryEmployee's
   *     listaEmployee.
   * @return this assertion object.
   * @throws AssertionError if the actual RepositoryEmployee's listaEmployee contains any given
   *     Employee elements.
   */
  public KataRepositoryEmployeeAssert doesNotHaveListaEmployee(Employee... listaEmployee) {
    // check that actual RepositoryEmployee we want to make assertions on is not null.
    isNotNull();

    // check that given Employee varargs is not null.
    if (listaEmployee == null) failWithMessage("Expecting listaEmployee parameter not to be null.");

    // check with standard error message (use overridingErrorMessage before contains to set your own
    // message).
    Iterables.instance().assertDoesNotContain(info, actual.getListaEmployee(), listaEmployee);

    // return the current assertion for method chaining
    return this;
  }

  /**
   * Verifies that the actual RepositoryEmployee's listaEmployee does not contain the given Employee
   * elements in Collection.
   *
   * @param listaEmployee the given elements that should not be in actual RepositoryEmployee's
   *     listaEmployee.
   * @return this assertion object.
   * @throws AssertionError if the actual RepositoryEmployee's listaEmployee contains any given
   *     Employee elements.
   */
  public KataRepositoryEmployeeAssert doesNotHaveListaEmployee(
      java.util.Collection<? extends Employee> listaEmployee) {
    // check that actual RepositoryEmployee we want to make assertions on is not null.
    isNotNull();

    // check that given Employee collection is not null.
    if (listaEmployee == null) {
      failWithMessage("Expecting listaEmployee parameter not to be null.");
      return this; // to fool Eclipse "Null pointer access" warning on toArray.
    }

    // check with standard error message (use overridingErrorMessage before contains to set your own
    // message).
    Iterables.instance()
        .assertDoesNotContain(info, actual.getListaEmployee(), listaEmployee.toArray());

    // return the current assertion for method chaining
    return this;
  }

  /**
   * Verifies that the actual RepositoryEmployee has no listaEmployee.
   *
   * @return this assertion object.
   * @throws AssertionError if the actual RepositoryEmployee's listaEmployee is not empty.
   */
  public KataRepositoryEmployeeAssert hasNoListaEmployee() {
    // check that actual RepositoryEmployee we want to make assertions on is not null.
    isNotNull();

    // we override the default error message with a more explicit one
    String assertjErrorMessage =
        "\nExpecting :\n  <%s>\nnot to have listaEmployee but had :\n  <%s>";

    // check
    if (actual.getListaEmployee().iterator().hasNext()) {
      failWithMessage(assertjErrorMessage, actual, actual.getListaEmployee());
    }

    // return the current assertion for method chaining
    return this;
  }
}
