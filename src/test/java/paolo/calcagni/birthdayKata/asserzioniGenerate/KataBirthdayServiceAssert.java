package paolo.calcagni.birthdayKata.asserzioniGenerate;

import org.assertj.core.api.AbstractObjectAssert;
import paolo.calcagni.birthdayKata.BirthdayService;

/** {@link BirthdayService} specific assertions - Generated by CustomAssertionGenerator. */
@javax.annotation.Generated(value = "assertj-assertions-generator")
public class KataBirthdayServiceAssert
    extends AbstractObjectAssert<KataBirthdayServiceAssert, BirthdayService> {

  /**
   * Creates a new <code>{@link KataBirthdayServiceAssert}</code> to make assertions on actual
   * BirthdayService.
   *
   * @param actual the BirthdayService we want to make assertions on.
   */
  public KataBirthdayServiceAssert(BirthdayService actual) {
    super(actual, KataBirthdayServiceAssert.class);
  }

  /**
   * An entry point for KataBirthdayServiceAssert to follow AssertJ standard <code>assertThat()
   * </code> statements.<br>
   * With a static import, one can write directly: <code>assertThat(myBirthdayService)</code> and
   * get specific assertion with code completion.
   *
   * @param actual the BirthdayService we want to make assertions on.
   * @return a new <code>{@link KataBirthdayServiceAssert}</code>
   */
  @org.assertj.core.util.CheckReturnValue
  public static KataBirthdayServiceAssert assertThat(BirthdayService actual) {
    return new KataBirthdayServiceAssert(actual);
  }
}
