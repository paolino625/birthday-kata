package paolo.calcagni.birthdayKata.asserzioniGenerate;

import paolo.calcagni.birthdayKata.RepositoryEmployeeReal;

/**
 * Entry point for assertions of different data types. Each method in this class is a static factory
 * for the type-specific assertion objects.
 */
public class Assertions extends org.assertj.core.api.Assertions {

  /**
   * Creates a new instance of <code>{@link KataBirthdayServiceAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static KataBirthdayServiceAssert assertThat(
      paolo.calcagni.birthdayKata.BirthdayService actual) {
    return new KataBirthdayServiceAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link KataEmployeeAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static KataEmployeeAssert assertThat(paolo.calcagni.birthdayKata.Employee actual) {
    return new KataEmployeeAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link KataMainAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static KataMainAssert assertThat(paolo.calcagni.birthdayKata.Main actual) {
    return new KataMainAssert(actual);
  }

  /**
   * Creates a new instance of <code>{@link KataRepositoryEmployeeAssert}</code>.
   *
   * @param actual the actual value.
   * @return the created assertion object.
   */
  @org.assertj.core.util.CheckReturnValue
  public static KataRepositoryEmployeeAssert assertThat(RepositoryEmployeeReal actual) {
    return new KataRepositoryEmployeeAssert(actual);
  }

  protected Assertions() {
    // empty
  }
}
