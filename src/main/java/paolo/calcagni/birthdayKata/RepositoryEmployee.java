package paolo.calcagni.birthdayKata;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

public interface RepositoryEmployee {
  ArrayList<Employee> getListaEmployee();

  void inizializzaRepository(BufferedReader in) throws IOException;
}
