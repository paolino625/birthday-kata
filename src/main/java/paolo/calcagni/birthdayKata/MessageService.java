package paolo.calcagni.birthdayKata;

public interface MessageService {

  void sendMessage(String subject, String body, String recipient);
}
