package paolo.calcagni.birthdayKata;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

public class RepositoryEmployeeReal implements RepositoryEmployee {

  private final ArrayList<Employee> listaEmployee = new ArrayList<Employee>();

  public void inizializzaRepository(BufferedReader in) throws IOException {

    listaEmployee.clear();

    String stringa = "";
    in.readLine(); // Salto l'header

    while ((stringa = in.readLine()) != null) {
      String[] employeeData = stringa.split(", ");
      Employee employee =
          new Employee(employeeData[1], employeeData[0], employeeData[2], employeeData[3]);
      listaEmployee.add(employee);
    }
  }

  public String listaEmployeeRepository() {

    String stringaFinale = "";

    for (Employee employee : listaEmployee) {

      stringaFinale +=
          "Nome: "
              + employee.getNome()
              + "\nCognome: "
              + employee.getCognome()
              + "\nData di Nascita: "
              + employee.getDataNascita()
              + "\nMail: "
              + employee.getEmail()
              + "\n";
    }

    return stringaFinale;
  }

  public ArrayList<Employee> getListaEmployee() {

    return listaEmployee;
  }
}
