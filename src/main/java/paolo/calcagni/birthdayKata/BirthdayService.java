package paolo.calcagni.birthdayKata;

import com.google.inject.Inject;
import java.time.LocalDate;

public class BirthdayService {

  RepositoryEmployee repositoryEmployee;
  MessageService messageService;

  @Inject
  public BirthdayService(MessageService messageService, RepositoryEmployee repositoryEmployee) {

    this.messageService = messageService;
    this.repositoryEmployee = repositoryEmployee;
  }

  public void sendGreetings(LocalDate data) {
    for (Employee employee : repositoryEmployee.getListaEmployee()) {
      if (employee.compleannoOggi(data)) {
        String recipient = employee.getEmail();
        String body = "Happy Birthday, dear %NAME%".replace("%NAME%", employee.getNome());
        String subject = "Happy Birthday!";
        messageService.sendMessage(subject, body, recipient);
      }
    }
  }
}
