package paolo.calcagni.birthdayKata;

import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MessageServiceReal implements MessageService {

  private final String host;
  private final int port;

  public MessageServiceReal(String host, int port) {
    this.host = host;
    this.port = port;
  }

  public void sendMessage(String object, String body, String recipient) {

    // Create a mail session
    Properties props = new Properties();
    props.put("mail.smtp.host", host);
    props.put("mail.smtp.port", "" + port);

    Session session = Session.getDefaultInstance(props, null);

    // Construct the message
    try {
      MimeMessage message = new MimeMessage(session);
      message.setFrom(new InternetAddress("tester@tester.com"));
      message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
      message.setSubject(object);
      message.setText(body);

      // send the message
      Transport.send(message);

    } catch (MessagingException e) {
      e.printStackTrace();
    }
  }
}
