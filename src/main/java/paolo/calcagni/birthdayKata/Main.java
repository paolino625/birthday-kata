package paolo.calcagni.birthdayKata;

import com.google.inject.Guice;
import com.google.inject.Injector;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;

public class Main {

  public static void main(String[] args) throws IOException {

    Injector injector = Guice.createInjector(new BirthdayServiceModule());
    MessageService messageService = injector.getInstance(MessageService.class);
    RepositoryEmployee repositoryEmployee = injector.getInstance(RepositoryEmployeeReal.class);

    String file = "src/main/resources/employee_data.txt";
    BufferedReader in = new BufferedReader(new FileReader(file));
    repositoryEmployee.inizializzaRepository(in);

    BirthdayService service = new BirthdayService(messageService, repositoryEmployee);

    service.sendGreetings(LocalDate.parse("2022-01-03"));
  }
}
