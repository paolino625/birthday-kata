package paolo.calcagni.birthdayKata;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public class BirthdayServiceModule extends AbstractModule {

  @Provides
  @Singleton
  MessageService messageService() {
    return new MessageServiceReal("localhost", 10001);
  }

  @Provides
  @Singleton
  RepositoryEmployee repositoryEmployee() {
    return new RepositoryEmployeeReal();
  }
}
