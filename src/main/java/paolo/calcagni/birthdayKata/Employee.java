package paolo.calcagni.birthdayKata;

import java.time.LocalDate;

public class Employee {

  private final String nome, cognome, email;
  private final LocalDate dataNascita;

  public Employee(String nome, String cognome, String dataNascita, String email) {

    this.nome = nome;
    this.cognome = cognome;
    this.dataNascita = LocalDate.parse(dataNascita);
    this.email = email;
  }

  public String getNome() {
    return nome;
  }

  public String getCognome() {
    return cognome;
  }

  public int getGiornoNascita() {
    return dataNascita.getDayOfMonth();
  }

  public int getMeseNascita() {
    return dataNascita.getMonthValue();
  }

  public int getAnnoNascita() {
    return dataNascita.getYear();
  }

  public String getEmail() {
    return email;
  }

  public String getDataNascita() {
    return getAnnoNascita() + "/" + getMeseNascita() + "/" + getGiornoNascita();
  }

  public Boolean compleannoOggi(LocalDate dataOdierna) {

    Boolean stessoGiorno = dataOdierna.getDayOfMonth() == getGiornoNascita();
    Boolean stessoMese = dataOdierna.getMonthValue() == getMeseNascita();

    return stessoGiorno && stessoMese;
  }

  public String toString() {

    return getNome() + " " + getCognome() + " " + getDataNascita() + " " + getEmail();
  }
}
