package paolo.calcagni.birthdayKata;

import static org.assertj.core.api.Assertions.assertThat;

import com.dumbster.smtp.SimpleSmtpServer;
import com.dumbster.smtp.SmtpMessage;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public class MyStepdefs {

  SimpleSmtpServer smtpServer;
  String data;

  Injector injector = Guice.createInjector(new BirthdayServiceModule());
  MessageService messageService = injector.getInstance(MessageService.class);
  RepositoryEmployee repositoryEmployee = injector.getInstance(RepositoryEmployeeReal.class);
  BirthdayService service = new BirthdayService(messageService, repositoryEmployee);

  @Before
  public void setupServerDummy() throws IOException {
    smtpServer = SimpleSmtpServer.start(10001);
  }

  @After
  public void tearDownServerDummy() {
    smtpServer.stop();
  }

  @Given("un CSV file che contiene i seguenti nominativi:")
  public void unCSVFileCheContieneISeguentiNominativi(String text) throws IOException {
    Reader reader = new StringReader(text);
    BufferedReader bufferedReader = new BufferedReader(reader);

    repositoryEmployee.inizializzaRepository(bufferedReader);
  }

  @And("oggi è il {string}")
  public void oggiÈIl(String stringa) {
    data = stringa;
  }

  @When("Chiediamo di mandare gli auguri ai nostri dipendenti")
  public void chiediamoDiMandareGliAuguriAiNostriDipendenti() {
    service.sendGreetings(LocalDate.parse(data));
  }

  @Then("Dovrebbero essere mandati gli auguri a:")
  public void dovrebberoEssereMandatiGliAuguriA(List<Map<String, String>> dataTable) {

    List<SmtpMessage> emails = smtpServer.getReceivedEmails();
    assertThat(emails.size()).isEqualTo(1);
    SmtpMessage email = emails.get(0);
    assertThat(email.getHeaderValue("Subject")).isEqualTo(dataTable.get(0).get("soggetto"));
    assertThat(email.getBody()).isEqualTo(dataTable.get(0).get("body"));
    assertThat(email.getHeaderValue("To")).isEqualTo(dataTable.get(0).get("email"));
  }
}
