Feature: birthday greetings test accettazione

  Scenario: primo scenario
    Given un CSV file che contiene i seguenti nominativi:
    """
    last_name, first_name, date_of_birth, email
    Calcagni, Paolo, 1997-01-03, paolo.calcagni@studenti.unimi.it
    Mary, Ann, 1975-03-11, mary.ann@foobar.com
    """
    And oggi è il "2020-01-03"
    When Chiediamo di mandare gli auguri ai nostri dipendenti
    Then Dovrebbero essere mandati gli auguri a:
      | email | soggetto | body |
      | paolo.calcagni@studenti.unimi.it | Happy Birthday! | Happy Birthday, dear Paolo |