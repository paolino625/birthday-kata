[![pipeline status](https://gitlab.com/paolino625/birthday-kata/badges/master/pipeline.svg)](https://gitlab.com/paolino625/birthday-kata/-/commits/master)
[![coverage report](https://gitlab.com/paolino625/birthday-kata/badges/master/coverage.svg)](https://gitlab.com/paolino625/birthday-kata/-/commits/master)

**INDICE PARZIALE BIRTHDAY KATA**

L'indice totale si può trovare nel repository triangles.

| Assignment  | Argomento Lezione | Task Svolti |
| ---------- | --------- | ------------------- |
| [Assignment8b](https://gitlab.com/paolino625/birthday-kata/-/tree/calcagni_paolo_assignment8b)  | Mockito. | Realizzato Kata Birthday adottando tecniche di mocking. |
| [Assignment10](https://gitlab.com/paolino625/birthday-kata/-/tree/calcagni_paolo_assignment10) | Guice. | Sostituito LocalDate a Gregorian Calendar (tolta classe personalizzata Data in quanto non più necessaria). Aggiunta suddivisione in package. Aggiunti pipeline e badge. Utilizzate la cartella "resources". Generate asserzioni personalizzate con AssertJ. Migliorata copertura dei metodi aggiungendo test. Sostituito Codecov a Pages. Implementato Guice. |
| [Assignment12](https://gitlab.com/paolino625/birthday-kata/-/tree/calcagni_paolo_assignment12)  | | Implementazione della classe RepositoryEmployeeReal e creata classe di test relativa.  |
| [Assignment14](https://gitlab.com/paolino625/birthday-kata/-/tree/calcagni_paolo_assignment14)  | Cucumber. | Migliorata implementazione di Juice (creato BirthdayServiceModule). Creati Cucumber Test. |
